package com.swang.security.sdk;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @ClassName: SwangUserDetails
 * @Description: 当前登录用户详情
 * @author swang
 * @date 2017年4月14日 下午1:33:00
 */
public class SwangUserDetails extends User implements UserDetails {
	private static final long serialVersionUID = -2920433765248963774L;
	
	private Map<String, GrantedAuthority> authorityMap;
	private boolean accountNonExpired;
	private boolean accountNonLocked;
	private boolean credentialsNonExpired;

	public SwangUserDetails() {}
	
	public Map<String, GrantedAuthority> getAuthorityMap() {
		return authorityMap;
	}

	public void setAuthorityMap(Map<String, GrantedAuthority> authorityMap) {
		this.authorityMap = authorityMap;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return authorityMap.values();
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		accountNonLocked =  getStatus()!=2;
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		if(CollectionUtils.isEmpty(authorities)){
			return;
		}
		authorityMap = new HashMap<String, GrantedAuthority>();
		for (GrantedAuthority auth : authorities) {
			if(!StringUtils.isBlank(auth.getAuthority())){
				authorityMap.put(auth.getAuthority().trim(), auth);
			}
		}
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		if(!accountNonLocked){
			setStatus(2);
		}
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	
    @Override
    public boolean equals(Object rhs) {
        if (rhs instanceof SwangUserDetails) {
            return getUsername().equals(((SwangUserDetails) rhs).getUsername());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getUsername().hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString()).append(": ");
        sb.append("Username: ").append(getUsername()).append("; ");
        sb.append("Password: [PROTECTED]; ");
        sb.append("Enabled: ").append(isEnabled()).append("; ");
        sb.append("AccountNonExpired: ").append(this.accountNonExpired).append("; ");
        sb.append("credentialsNonExpired: ").append(this.credentialsNonExpired).append("; ");
        sb.append("AccountNonLocked: ").append(this.accountNonLocked).append("; ");
        if (!authorityMap.isEmpty()) {
            sb.append("Granted Authorities: ");
            boolean first = true;
            for (GrantedAuthority auth : authorityMap.values()) {
                if (!first) {
                    sb.append(",");
                }
                first = false;
                sb.append(auth);
            }
        } else {
            sb.append("Not granted any authorities");
        }
        return sb.toString();
    }

}
