package com.swang.security.sdk;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * @ClassName: SecurityContext
 * @Description: 当前登录用户上下文
 * @author swang
 * @date 2017年4月14日 上午10:34:30
 */
public class SecurityContext  {
	private static final Logger logger = Logger.getLogger(SecurityContext.class);
	
	public static final String ANONYMOUS_USER = "anonymousUser";
	
	/**
	 * 获取当前用户的认证对象
	 * @author swang
	 * @date 2017年4月23日 下午3:50:41
	 * @return
	 */
	public static Authentication getCurrentAuthentication(){
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	/**
	 * 当前用户是否认证过
	 * @author swang
	 * @date 2017年4月23日 下午3:59:00
	 * @return
	 */
	public static boolean isAuthenticated(){
		return getCurrentAuthentication().isAuthenticated();
	}

	/**
	 * 获得当前用户
	 * User currentUser = SecurityContext.currentUser();
	 * @return User 当前用户
	 */
	public static User currentUser() {
		Object principal = getCurrentAuthentication().getPrincipal();
		if(ANONYMOUS_USER.equals(principal)){
			if(logger.isInfoEnabled()){
				logger.info("匿名用户");
			}
			return null;
		}
		User currentUser = null;
		try {
			currentUser = (User) principal;
		} catch (ClassCastException e) {
			logger.error("获取当前用户信息转换异常", e);
			return null;
		}
		return currentUser;
	}
	
	/**
	 * 判断当前用户是否匿名用户
	 * @author swang
	 * @date 2017年4月23日 下午3:40:11
	 * @return
	 */
	public static boolean isAnonymousUser(){
		Object principal = getCurrentAuthentication().getPrincipal();
		return ANONYMOUS_USER.equals(principal);
	}
	
	/**
	 * 获取当前用户拥有的权限集合
	 * @author swang
	 * @date 2017年4月23日 下午4:30:48
	 * @return
	 */
	public static Set<String> getAuthorities(){
		Object principal = getCurrentAuthentication().getPrincipal();
		if(ANONYMOUS_USER.equals(principal)){
			return null;
		}
		SwangUserDetails user = (SwangUserDetails) principal;
		Map<String, GrantedAuthority> authorityMap = user.getAuthorityMap();
		if(authorityMap == null || authorityMap.size()<=0){
			return null;
		}
		return authorityMap.keySet();
	}

	/**
	 * 判断当前用户是否有给定的权限
	 * 注意：此方法在应用层使用，如在Controller,Service中使用
	 * if ( SecurityContext.authorize( "ROLE_ADMIN" ) )
	 * @param authority
	 * @return
	 */
	public static boolean hasRole(final String authority) {
		Object principal = getCurrentAuthentication().getPrincipal();
		if(ANONYMOUS_USER.equals(principal)){
			return false;
		}
		SwangUserDetails user = (SwangUserDetails) principal;
		Map<String, GrantedAuthority> authorityMap = user.getAuthorityMap();
		if(authorityMap == null || authorityMap.size()<=0){
			return false;
		}
		return authorityMap.get(authority)!=null;
	}
	
}
