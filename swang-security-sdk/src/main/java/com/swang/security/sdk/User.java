package com.swang.security.sdk;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: User
 * @Description: 平台系统用户
 * @author swang
 * @date 2017年4月14日 上午10:42:17
 */
public class User implements Serializable {
	private static final long serialVersionUID = 6427496640721395199L;

	/** 对应数据库的主键 */
	private Long userId;
	
	/** 登录类型，"user_name","phone_no","email","extend_auth" */
	private String loginType;
	
	/** 登录用户名 */
	private String username;

	/** 登录密码 */
	private String password;
	
	/** 密码盐值 */
	private String salt;
	
	/** 用户状态（1=正常、2=冻结、3=注销）；注销表示用户被注销不能用，但是该用户数据在统计范围之内 */
	private Integer status;
	
	/** 是否启用,1启用 0停用；停用所有数据将失效包括统计数据 */
	private boolean enabled;
	
	/** 用户头像 */
	private String headPhoto;
	
	/** 昵称 */
	private String nickName;
	
	/** 用户终端类型。1为浏览器（包括PC浏览器、手机浏览器）；2为Android客户端；3位IOS客户端；4为支付宝服务窗；5为微信公账号；6为QQ终端 */
	private Integer terminalType;
	
	/** 客户端应用账户 */
	private String appAuthName;
	
	/** 人员Id */
	private Long personId;
	
	/** 默认为0，0代表普通开发者，1代表组织超级管理员，2代表系统超级管理员 */
	private int userType;
	
	/** 普通用户 */
	public static final int USER_ORDINARY = 0;
	/** 组织超级管理员 */
	public static final int USER_ORG_SUPER = 1;
	/** 系统超级管理员 */
	public static final int USER_SYS_SUPER = 2;
	
	/** 普通用户角色 */
	public static final String ROLE_ORDINARY = "ROLE_ORDINARY";
	/** 组织超级管理员角色 */
	public static final String ROLE_ORG_SUPER = "ROLE_ORG_SUPER_";
	/** 系统超级管理员角色 */
	public static final String ROLE_SYS_SUPER = "ROLE_SUPER";
	
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public boolean isEnabled() {
		if(enabled && (status==1 || status==2)){
			return true;
		}
		if(!enabled || status==3){
			return false;
		}
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getHeadPhoto() {
		return headPhoto;
	}
	public void setHeadPhoto(String headPhoto) {
		this.headPhoto = headPhoto;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Integer getTerminalType() {
		return terminalType;
	}
	public void setTerminalType(Integer terminalType) {
		this.terminalType = terminalType;
	}
	public String getAppAuthName() {
		return appAuthName;
	}
	public void setAppAuthName(String appAuthName) {
		this.appAuthName = appAuthName;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
