package com.swang.security.sdk;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: Config
 * @Description: 集成swang-security-sdk的配置参数
 * @author swang
 * @date 2017年4月18日 下午7:58:05
 */
public class Config implements Serializable {
	private static final long serialVersionUID = 4382441353715165948L;
	public static final String localAppAuthName = "common";

	/** 资源服务器ID */
	private String resourceServerId;

	/** swang-security-sdk调用的zk地址 */
	private String dubboRegistry;
	/** dubbo超时时间 */
	private Integer dubboTimeOut = 60000;
	/** dubbo服务分组，默认返回"dubbo" */
	private String dubboGroup = CommonConstants.DUBBO;
	
	/** 应用服务名，默认为common */
	private String appAuthName = localAppAuthName;
	/** 应用密码 */
	private String appAuthSecret;


	public String getResourceServerId() {
		return resourceServerId;
	}

	public void setResourceServerId(String resourceServerId) {
		this.resourceServerId = resourceServerId;
	}

	public String getDubboRegistry() {
		return dubboRegistry;
	}

	public void setDubboRegistry(String dubboRegistry) {
		this.dubboRegistry = dubboRegistry;
	}

	public Integer getDubboTimeOut() {
		return dubboTimeOut;
	}

	public void setDubboTimeOut(Integer dubboTimeOut) {
		this.dubboTimeOut = dubboTimeOut;
	}

	public String getDubboGroup() {
		return dubboGroup;
	}

	public void setDubboGroup(String dubboGroup) {
		this.dubboGroup = dubboGroup;
	}

	public String getAppAuthName() {
		return appAuthName;
	}

	public void setAppAuthName(String appAuthName) {
		this.appAuthName = appAuthName;
	}

	public String getAppAuthSecret() {
		return appAuthSecret;
	}

	public void setAppAuthSecret(String appAuthSecret) {
		this.appAuthSecret = appAuthSecret;
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
