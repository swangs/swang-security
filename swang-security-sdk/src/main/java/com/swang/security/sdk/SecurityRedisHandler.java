package com.swang.security.sdk;

import java.util.Set;

import org.springframework.security.core.session.SessionInformation;

/**
 * @ClassName: SecurityRedisHandler
 * @Description: spring security的session的redis操作
 * @author swang
 * @date 2017年4月15日 下午2:08:32
 */
public interface SecurityRedisHandler extends SecurityRedisKey {
	
	Set<String> getAllPrincipals();

	Set<String> getAllSessionIds(Object principal);
	
	SessionInformation getSessionInformation(String sessionId);
	
	void savePrincipalSessionId(Object principal, String sessionId);
	
	void savePrincipalKey(Object principal);
	
	void saveSessionInfoBySessoinId(String sessionId, SessionInformation sessionInfo);
	
	void removeSessionInfo(String sessionId);
	
	Long removePrincipalSessionId(Object principal, String sessionId);
	
	void removePrincipalKey(Object principal);
	
	Long countPrincipalSessionId(Object principal);
	
}
