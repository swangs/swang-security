package com.swang.security.sdk.integration;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.swang.security.facade.UserFacade;
import com.swang.security.facade.result.UserDetailResult;
import com.swang.security.facade.rpc.CommonResult;
import com.swang.security.sdk.LoggerUtils;
import com.swang.security.sdk.User;

/**
 * @ClassName: SdkSecurityClient
 * @Description: RPC服务访问，访问swang-security-facade
 * @author swang
 * @date 2017年4月18日 下午8:47:30
 */
public class SdkSecurityClient implements InitializingBean {
	private static final Logger logger = Logger.getLogger(SdkSecurityClient.class);
	
	private UserFacade userFacade;

	public void setUserFacade(UserFacade userFacade) {
		this.userFacade = userFacade;
	}
	
	/**
	 * 根据用户输入的用户名查找库中的用户信息
	 * @author swang
	 * @date 2017年4月17日 下午7:56:51
	 * @param userName	用户输入的用户名，可能是user_name、phone_no、email、extend_auth中的一种
	 * @return
	 */
	public User findUserByUserName(String userName){
		CommonResult<UserDetailResult> result = userFacade.findUserByUserName(userName);
		LoggerUtils.debug(logger, "SdkSecurityClient.findUserByUserName入参：{0}", userName);
		LoggerUtils.debug(logger, "返回参数：{0}", result);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			UserDetailResult udr = result.getContent();
			User user = new User();
			BeanUtils.copyProperties(udr, user, "loginType","terminalType","appAuthName","userType");
			return user;
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.userFacade, "UserFacade注入不能为空");
	}
	
}
