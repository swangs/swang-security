package com.swang.security.sdk.integration;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.swang.redis.facade.RedisFacade;
import com.swang.redis.facade.rpc.CommonResult;
import com.swang.redis.facade.rpc.Result;

/**
 * @ClassName: SingleSpringRedisClient
 * @Description: 单实例redis服务的封装访问
 * @author swang
 * @date 2017年4月23日 下午8:12:23
 */
public class SingleSpringRedisClient implements RedisClient {
	private final Log logger = LogFactory.getLog(getClass());

	private RedisFacade redisFacade;
	
	public void setRedisFacade(RedisFacade redisFacade) {
		this.redisFacade = redisFacade;
	}

	@Override
	public String get(String key) {
		CommonResult<String> result = redisFacade.get(key);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			return result.getContent();
		}
	}

	@Override
	public Set<String> smembers(String key) {
		CommonResult<Set<String>> result = redisFacade.smembers(key);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			return result.getContent();
		}
	}

	@Override
	public Long sadd(String key, String... value) {
		CommonResult<Long> result = redisFacade.sadd(key, value);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			return result.getContent();
		}
	}

	@Override
	public void set(String key, String value) {
		Result result = redisFacade.set(key, value);
		if (result == null || !result.getIsSuccess()) {
			logger.error("SdkJedisClient.set(String key, String value)发生异常！入参：key="+key+",value="+value+"。返回结果："+result);
		}
	}

	@Override
	public void expire(String key, int seconds) {
		Result result = redisFacade.expire(key, seconds);
		if (result == null || !result.getIsSuccess()) {
			logger.error("SdkJedisClient.expire(String key, int seconds)发生异常！入参：key="+key+",seconds="+seconds+"。返回结果："+result);
		}
	}

	@Override
	public Long srem(String key, String... value) {
		CommonResult<Long> result = redisFacade.srem(key, value);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			return result.getContent();
		}
	}

	@Override
	public Long scard(String key) {
		CommonResult<Long> result = redisFacade.scard(key);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			return result.getContent();
		}
	}

}
