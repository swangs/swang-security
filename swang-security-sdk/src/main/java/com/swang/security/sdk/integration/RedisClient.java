package com.swang.security.sdk.integration;

import java.util.Set;

/**
 * @ClassName: RedisClient
 * @Description: 连接redis RPC暴露接口
 * @author swang
 * @date 2017年4月23日 下午8:08:48
 */
public interface RedisClient {

	String get(String key);
	
	Set<String> smembers(String key);
	
	Long sadd(String key, String... value);
	
	void set(String key, String value);
	
	void expire(String key, int seconds);
	
	Long srem(String key, String... value);
	
	Long scard(String key);
	
}
