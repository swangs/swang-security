package com.swang.security.sdk;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * @ClassName: SwangLogoutSuccessHandler
 * @Description: 退出成功的操作
 * @author swang
 * @date 2017年4月14日 下午6:31:14
 */
public class SwangLogoutSuccessHandler extends AbstractAuthenticationTargetUrlRequestHandler implements LogoutSuccessHandler {

	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		// 此处可以做一些退出成功的业务
		
		super.handle(request, response, authentication);
	}

}
