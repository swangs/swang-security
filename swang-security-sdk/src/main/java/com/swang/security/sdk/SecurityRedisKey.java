package com.swang.security.sdk;

/**
 * @ClassName: SecurityRedisKey
 * @Description: spring-security的session管理相关的key
 * @author swang
 * @date 2017年4月15日 上午11:18:08
 */
public interface SecurityRedisKey extends CommonConstants {

	/**
	 * 用于保存spring-security的session管理中所有用户信息(appAuthName@userId)的key的集合 list
	 * 分布式存储
	 */
	String SWANG_SECURITY_SESSIONMANAGE_PRINCIPAL_KEY = "SWANG:SECURITY:SESSIONMANAGE:PRINCIPAL:KEY";

	/**
	 * 用于保存spring-security的session管理中一个用户对应的所有的sessionId
	 * {0}对应一个产品的一个用户（appAuthName@userId） 唯一 list集合
	 * 分布式存储
	 */
	String SWANG_SECURITY_SESSIONMANAGE_PRINCIPAL_SESSIONID = "SWANG:SECURITY:SESSIONMANAGE:PRINCIPAL:SESSIONID:{0}";

	/**
	 * 用于保存spring-security的session管理中 保存每个session的详细信息，通过sessionId进行对应 String
	 * 分布式存储
	 */
	String SWANG_SECURITY_SESSIONMANAGE_SESSIONID_SESSIONINFO = "SWANG:SECURITY:SESSIONMANAGE:SESSIONID:SESSIONINFO:{0}";
	
}
