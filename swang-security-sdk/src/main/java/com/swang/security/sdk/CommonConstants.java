package com.swang.security.sdk;

/**
 * @ClassName: CommonConstants
 * @Description: 常用公共常量
 * @author swang
 * @date 2017年4月15日 上午11:33:57
 */
public interface CommonConstants {

	/** [@]分隔符 */
	String CLIENTSPLIT = "[@]";

	/** @分隔符 */
	String ADDCLIENTSPLIT = "@";

	/** redis中通过过期删除一个key的时间 */
	Integer REDISDELTIME = 0;

	/** long 的0常量 */
	Long LZERO = 0l;
	
	/** dubbo */
	String DUBBO = "dubbo";
	
}
