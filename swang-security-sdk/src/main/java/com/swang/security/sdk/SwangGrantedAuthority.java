package com.swang.security.sdk;

import org.springframework.security.core.GrantedAuthority;

/**
 * @ClassName: SwangGrantedAuthority
 * @Description: 角色权限资源授权体
 * @author swang
 * @date 2017年4月16日 下午1:16:41
 */
public class SwangGrantedAuthority implements GrantedAuthority {
	private static final long serialVersionUID = -1194969608168682514L;

	private String authority;
	
	public SwangGrantedAuthority() {
	}
	
	public SwangGrantedAuthority(String authority) {
		this.authority = authority;
	}
	
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
	@Override
	public String getAuthority() {
		return authority;
	}

	@Override
	public boolean equals(Object obj) {
        if (obj instanceof String) {
            return obj.equals(this.authority);
        }
        if (obj instanceof GrantedAuthority) {
            GrantedAuthority attr = (GrantedAuthority) obj;
            return authority.equals(attr.getAuthority());
        }
        return false;
	}
	
	@Override
    public int hashCode() {
        return authority.hashCode();
    }

	@Override
    public String toString() {
        return authority;
    }
	
}
