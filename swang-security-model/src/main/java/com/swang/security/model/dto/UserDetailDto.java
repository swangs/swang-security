package com.swang.security.model.dto;

import java.io.Serializable;

/**
 * @ClassName: UserDetailDto
 * @Description: sql入参
 * @author swang
 * @date 2017年4月18日 上午12:09:05
 */
public class UserDetailDto implements Serializable {
	private static final long serialVersionUID = -3727421469845180295L;

	private String userName;
	
	private Long phoneNo;
	
	public UserDetailDto(){}
	
	public UserDetailDto(String userName, Long phoneNo) {
		this.userName = userName;
		this.phoneNo = phoneNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(Long phoneNo) {
		this.phoneNo = phoneNo;
	}
	
}
