package com.swang.security.model.vo;

import java.io.Serializable;

public class TestVo implements Serializable {
	private static final long serialVersionUID = -1527141671033773591L;

	private String username;
	
	private String password;
	
	private Integer age;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
}
