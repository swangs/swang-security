package com.swang.security.model.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: PmsUser
 * @Description: 用户
 * @author swang
 * @date 2017年4月17日 下午5:11:02
 */
public class PmsUser implements Serializable {
	private static final long serialVersionUID = 2973268672819340235L;
	
	/** 用户ID */
	private Long id;
	/** 普通登录的用户名；由字母数字下划线组成，不允许~！@#￥%&*等特殊符号 */
	private String userName;
	/** 登录的手机号码 */
	private Long phoneNo;
	/** 可登录的邮箱 */
	private String email;
	/** 扩展认证字段，可存储多个字段的值，由''&''组合约定而成；一般格式为：{业务字段1}&{业务字段2}&…&{业务类型}，例如：{bizNo}&{bizType}&biz */
	private String extendAuth;
	/** 密码 */
	private String password;
	/** 密码盐值 */
	private String salt;
	/** 头像照 */
	private String headPhoto;
	/** 昵称 */
	private String nickName;
	/** 用户状态（1=正常、2=冻结、3=注销）；注销表示用户被注销不能用，但是该用户数据在统计范围之内 */
	private Integer status;
	/** 是否启用,1启用 0停用；停用所有数据将失效包括统计数据 */
	private Boolean enabled;
	/** 注册时的ip */
	private String ipAddress;
	/** 注册时的客户端代理信息 */
	private String userAgent;
	/** 注册时的用户终端类型。默认0为手动添加注册；1为浏览器注册（包括PC浏览器、手机浏览器）；2为Android客户端注册；3位IOS客户端注册；4为支付宝服务窗注册；5为微信公账号注册；6为QQ终端注册 */
	private Integer terminalType;
	/** 用户的默认角色 */
	private Long roleId;
	/** 注册时使用的应用客户端ID */
	private String appId;
	/** 所属人员，一个人员可以有多个用户 */
	private Long personId;
	/** 创建时间 */
	private Date gmtCreate;
	/** 最后更新时间 */
	private Date gmtModify;
	/** 最后一次添加或编辑此数据的用户名称 */
	private String operator;
	/** 最后一次添加或编辑此数据的用户的关联用户ID（user_id） */
	private Long operatorId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(Long phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExtendAuth() {
		return extendAuth;
	}
	public void setExtendAuth(String extendAuth) {
		this.extendAuth = extendAuth;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getHeadPhoto() {
		return headPhoto;
	}
	public void setHeadPhoto(String headPhoto) {
		this.headPhoto = headPhoto;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public Integer getTerminalType() {
		return terminalType;
	}
	public void setTerminalType(Integer terminalType) {
		this.terminalType = terminalType;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public Date getGmtCreate() {
		return gmtCreate;
	}
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	public Date getGmtModify() {
		return gmtModify;
	}
	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Long getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}
}
