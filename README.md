认证授权及鉴权
1. 统一认证服务（web）
2. 鉴权RPC服务
3. 鉴权SDK（供rest资源服务使用，配置http拦截）
4. 鉴权SDK（供rpc服务端使用，配置ioc拦截）
5. 两种鉴权SDK统一头参数SwangHeaderParam
6. 头参数
    method        方法名，类似com.swang.*这样的定义
    version       方法的版本号
    clientId      客户端id
    clientVersion 客户端版本号
    clientType    客户端类别
    accessToken   访问令牌，根据method及clientId字段组合来判断是否需要校验用户访问的合法性
    signature     签名，对body参数json字符串的签名，通过method及clientId字段组合来判断是否需要校验签名