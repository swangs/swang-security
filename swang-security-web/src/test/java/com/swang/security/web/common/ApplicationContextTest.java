package com.swang.security.web.common;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * @ClassName: ApplicationContextTest
 * @Description: 自定义测试基类，其他测试类均可继承此基类
 * @author swang
 * @date 2017年4月9日 下午3:59:53
 */
//@ContextConfiguration(locations = { "classpath:/spring/spring.xml", "classpath:/spring/servlet-context.xml" })
@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class ApplicationContextTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	public ApplicationContextTest(){
		System.out.println("2. 第二步执行");
	}
	
	@BeforeClass
	public static void setUpClass() throws Exception {
		System.out.println("1. 第一步执行");
	}
	
	@AfterClass
	public static void tearDownClass() throws Exception {
		System.out.println("5. 第五步执行");
	}
	
	@Before
	public void setUp() {
		System.out.println("3. 第三步执行");
	}
	
	@After
	public void tearDown() {
		System.out.println("4. 第四步执行");
	}
	
	@Test
	public void testMapping() {
		System.out.println("【测试正文内容】测试基类，在第三步与第四步之间执行");
	}
	
}
