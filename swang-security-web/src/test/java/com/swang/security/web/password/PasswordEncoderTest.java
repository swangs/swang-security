package com.swang.security.web.password;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

public class PasswordEncoderTest {

	
	@Test
	public void testNewPasswordEncoder() throws Exception{
		String rawPwd = "wyl@123456";
		String salt = genUUID();
		String encriptPwd = this.genEncriptPwd(rawPwd, salt);
		
		System.out.println("明文密码："+rawPwd);
		System.out.println("密码盐值："+salt);
		System.out.println("生成加密密码："+encriptPwd);
		System.out.println("明文加密密码校验："+matchPassword(rawPwd, salt, encriptPwd));
	}
	
	
	
	
	private String genEncriptPwd(String rawPwd, String salt){
		if(StringUtils.isBlank(salt)){
			salt = "";
		}
		PasswordEncoder passwordEncoder = new StandardPasswordEncoder(salt);
		String encodedPassword = passwordEncoder.encode(rawPwd);
		return encodedPassword;
	}
	private boolean matchPassword(String rawPwd, String salt, String encriptPwd) throws Exception{
		if(StringUtils.isBlank(rawPwd) || StringUtils.isBlank(encriptPwd)){
			throw new Exception("密码校验参数有误！请检查！");
		}
		PasswordEncoder passwordEncoder = new StandardPasswordEncoder(salt);
		return passwordEncoder.matches(rawPwd, encriptPwd);
	}
	protected String genUUID(){
		return UUID.randomUUID().toString().replace("-", "");
	}
}
