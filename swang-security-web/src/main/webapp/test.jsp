<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="WEB-INF/jsp/common/common.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
	<title>欢迎您！</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="${contextPath}/js/jQuery/jquery-1.9.1.min.js" type="text/javascript" charset="UTF-8"></script>
	<!--[if lt IE 9]>
		<script src="${contextPath}/js/json2.js" type="text/javascript" charset="UTF-8"></script>
	<![endif]-->
	<script type="text/javascript">
	$(function(){
		$('#testRequestBody').click(function(){
			var url = globalContextPath + '/r/testRequestBody';
			$.ajax({
				url: url,
				type: 'post',
				contentType:"application/json",	//入参必须是json字符串
				//data:'RequestBody测试数据',    // not ok
				data:'{aa:12,bb:43}',			// ok
				success: function (data) {
					$('#resultContent').html(data);
			    }
			});
		});
		$('#testRequestBody2').click(function(){
			var url = globalContextPath + '/r/testRequestBody';
			$.ajax({
				url: url,
				type: 'post',
				contentType:"text/xml",
				data:'RequestBody测试数据',
				success: function (data) {
					$('#resultContent').html(data);
			    }
			});
		});
		$('#testResonseBody').click(function(){
			var url = '${contextPath}/r/testResonseBody/15067173438';
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data:{
					username : '王勇琳',
					password : '123456'
				},
				success: function (data) {
					$('#resultContent').html(JSON.stringify(data));
			    }
			});
		});
		$('#testDubboService').click(function(){
			var url = '${contextPath}/r/testDubbo';
			$.ajax({
				url: url,
				type: 'post',
				contentType:"application/json",
				data:JSON.stringify({
					username : '王勇琳',
					password : 'dddddddddddd',
					age : $('#testDubboService_age').val()
				}),
				success: function (data) {
					$('#resultContent').html(JSON.stringify(data));
			    }
			});
		});
		$('#testStandardInterface').click(function(){
			var url = '${contextPath}/r/testStandardInterface';
			$.ajax({
				url: url,
				type: 'post',
				contentType:"application/json",
				data:JSON.stringify({
					username : '王勇琳',
					password : 'fdsfdsfdsed',
					age : 25
				}),
				success: function (data) {
					$('#resultContent').html(JSON.stringify(data));
			    }
			});
		});
	});
	</script>
</head>
<body>
	欢迎您！<br/>
	${contextPath}<br/>
	${basePath}<br/>
	<a href="${contextPath}/r/testJsp?testData=您好" target="blank">JSP测试</a><br/>
	<a href="#" id="testRequestBody">测试RequestBody （application/json）</a><br/>
	<a href="#" id="testRequestBody2">测试RequestBody2 （text/xml）</a><br/>
	<a href="#" id="testResonseBody">测试ResonseBody （application/x-www-form-urlencoded）</a><br/><br/>

	age输入100测试RPC普通异常堆栈信息传输；age输入99测试RPC自定义异常追根溯源；age默认45为普通dubbo测试，预期age值变成88<br/>
	age：<input id="testDubboService_age" type="text" value="45"/><br/>
	<a href="#" id="testDubboService">测试dubbo服务  （application/json）</a><br/><br/>

	<a href="#" id="testStandardInterface">测试标准接口模板  （application/json）</a><br/>
	<br/>
	测试结果：<br/>
	<div id="resultContent"></div>
</body>
</html>
