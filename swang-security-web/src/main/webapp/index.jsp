<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="WEB-INF/jsp/common/common.jsp" %>

<security:authorize access="isAuthenticated()">
	
	<security:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_SUPER')">
		<c:redirect url="/login/index"/>
	</security:authorize>
	
	<%-- 没有分配主菜单的用户 --%>
	<c:redirect url="/403.jsp"/>
	
</security:authorize>

<c:redirect url="/jsp/login/login.jsp"/>


