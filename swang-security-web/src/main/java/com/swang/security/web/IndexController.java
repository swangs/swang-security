package com.swang.security.web;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/index")
public class IndexController {
	private static final Logger logger = Logger.getLogger(TestController.class);
    
    @RequestMapping
	public String first(){
        
		return "login/login";
	}

    @RequestMapping(value = "/page")
    public ModelAndView openPage(ModelMap model)  {
        return new ModelAndView("index/indexMgr",model);
    }

}
