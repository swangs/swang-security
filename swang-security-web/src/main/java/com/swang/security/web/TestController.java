package com.swang.security.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.swang.security.model.dto.TestDto;
import com.swang.security.model.vo.TestVo;
import com.swang.security.service.test.TestService;

/**
 * @ClassName: TestController
 * @Description: 测试使用
 * @author swang
 * @date 2017年4月10日 上午12:11:13
 */
@Controller
@RequestMapping("/r")
public class TestController {

	private static final Logger logger = Logger.getLogger(TestController.class);

	@Resource(name="testServiceImpl")
	private TestService testService;

	@RequestMapping(value="/testJsp", method=RequestMethod.GET)
	public String testJsp(String testData, Model model){
		model.addAttribute("data", testData);
		return "/test/testJsp";
	}

	@RequestMapping(value="/testRequestBody", method=RequestMethod.POST)
	public String testRequestBody(@RequestBody String testData, Model model){
		model.addAttribute("data", testData+"---ok");
		return "/test/testRequestBody";
	}

	@RequestMapping(value="/testResonseBody/{phone:\\d{11}+}", method=RequestMethod.POST)
	@ResponseBody
	public Object testResonseBody(@PathVariable final long phone, String username, String password, Model model){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("username", username);
		result.put("password", password);
		result.put("phone", phone);
		result.put("sex", "男");
		return result;
	}

	@RequestMapping(value="/testDubbo", method=RequestMethod.POST)
	@ResponseBody
	public Object testDubbo(@RequestBody TestDto dto){
		TestVo newVo = null;
		try {
			newVo = testService.findResultVo(dto);
		} catch (Exception e) {
			logger.error("dubbo调用发生异常！入参为："+ JSON.toJSONString(dto), e);
			return null;
		}
		return newVo;
	}

	/**
	 * 接口模板，入参、出参、日志规范
	 * 【注意】日志打印最好使用LoggerUtil来打印，不允许直接使用logger.debug()，如果强行使用需要加上if判断条件
	 * @param vo
	 * @return
	 */
	@RequestMapping(value="/testStandardInterface", method=RequestMethod.POST)
	@ResponseBody
	public TestVo testStandardInterface(@RequestBody TestVo vo){
		TestVo newVo = new TestVo();
		try {
			// 第一步，接口参数校验
			if(StringUtils.isBlank(vo.getUsername())){
				return null;
			}
			if(StringUtils.isBlank(vo.getPassword())){
				return null;
			}
			if(null == vo.getAge()){
				return null;
			}
			if(vo.getAge()>150 || vo.getAge()<0){
				return null;
			}
			// 第二步，业务调用处理
			newVo.setUsername("new username!"+vo.getUsername());
			newVo.setPassword("new password!"+vo.getPassword());
			newVo.setAge(vo.getAge()+10);
		} catch (Exception e) {
			//错误日志必须打印；注意，入参不能缺少，堆栈信息 e 必须打印
			logger.error("【模板接口】发生异常！入参为："+JSON.toJSONString(vo), e);
			return null;
		}
		//此处日志级别看调试的接口重要级别，重要接口可以为info，一般接口统一使用debug
		if(logger.isDebugEnabled()){
			logger.debug("【模板接口】入参为："+JSON.toJSONString(vo)+"，返回结果为："+JSON.toJSONString(newVo));
		}
		return newVo;
	}

}
