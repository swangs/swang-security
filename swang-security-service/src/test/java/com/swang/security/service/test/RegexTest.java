package com.swang.security.service.test;

import org.junit.Test;

public class RegexTest {

	//	/^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/
	private static final String emailRegex = "(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})";
	//	/^(\w)+(\&\w+)+$/
	//private static final String extendAuthRegex = "(\\w)+(\\&\\w+)+";
	private static final String extendAuthRegex = "(\\w)+(\\.\\w+)*(\\&(\\w)+(\\.\\w+)*)+";
	
	private static final String phoneRegex = "1[3-8][0-9]\\d{8}";
	
	@Test
	public void testEmailRegex(){
		System.out.println("f7d8a9f7".matches(emailRegex));
		System.out.println("f7d8a9f7@126.com".matches(emailRegex));
		System.out.println("f7d8a9.f7@126.com".matches(emailRegex));
		System.out.println("f7d8a9f7@".matches(emailRegex));
		System.out.println("f7d8a9f7@@126.com".matches(emailRegex));
	}
	
	@Test
	public void testExtendAuthRegex(){
		System.out.println("f7d8a9f7".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&126.com".matches(extendAuthRegex));
		System.out.println("f7d8.a9f7&126.com".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&fda7987".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&fd.a7987".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&fd-a7987".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&fd_a7987".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&126.com&fd8a90".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&".matches(extendAuthRegex));
		System.out.println("&f7d8a9f7".matches(extendAuthRegex));
		System.out.println("f7d8a9f7&&126.com".matches(extendAuthRegex));
	}
	
	@Test
	public void testPhoneRegex(){
		System.out.println("f7d8a9f7".matches(phoneRegex));
		System.out.println("12844444444".matches(phoneRegex));
		System.out.println("13396053392".matches(phoneRegex));
		System.out.println("10343432445".matches(phoneRegex));
		
	}
	
}
