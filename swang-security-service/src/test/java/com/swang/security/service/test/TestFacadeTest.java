package com.swang.security.service.test;

import javax.annotation.Resource;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.swang.security.facade.TestFacade;
import com.swang.security.facade.param.TestParam;
import com.swang.security.facade.rpc.Result;
import com.swang.security.service.common.ApplicationContextTest;

public class TestFacadeTest extends ApplicationContextTest {
	
	@Resource(name="testFacadeImpl")
	private TestFacade testFacade;

	@Test
	public void doTestResultForRedis() {
		TestParam param = new TestParam("xiaowang", "abc123", 18);
		Result result = testFacade.doTestResult(param);
		System.out.println(JSON.toJSONString(result));
	}
	
}
