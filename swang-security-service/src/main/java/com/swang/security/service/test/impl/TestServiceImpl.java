package com.swang.security.service.test.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swang.security.integration.test.TestClient;
import com.swang.security.model.dto.TestDto;
import com.swang.security.model.vo.TestVo;
import com.swang.security.service.test.TestService;

@Service
public class TestServiceImpl implements TestService {
	
	@Autowired
	private TestClient testClient;

	@Override
	public TestVo findResultVo(TestDto dto) {
		return testClient.findResultVo(dto);
	}

}
