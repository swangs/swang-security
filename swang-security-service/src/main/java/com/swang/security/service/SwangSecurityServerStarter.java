package com.swang.security.service;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
* @ClassName: SwangSecurityServerStarter
* @Description: 服务启动入口
* @author swang
* @date 2017年4月7日 下午3:38:49
*/
public class SwangSecurityServerStarter {
	
	private final static Logger logger = Logger.getLogger(SwangSecurityServerStarter.class);
	
	private final static String serverName = "swang-security-service";
	
	private static volatile boolean running = true;
	
	public static void main(String[] args) {
		try {
			logInfo(serverName + "服务启动中 …… ");
			final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("security-service-spring-context.xml");
			context.start();
			logInfo(serverName + "服务启动成功。");
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					context.close();
					logInfo(serverName+"服务停止运行！");
					synchronized (SwangSecurityServerStarter.class) {
						running = false;
						SwangSecurityServerStarter.class.notifyAll();
					}
				}
			});
		} catch (RuntimeException re) {
			String errorMsg = serverName + "服务启动异常 ！" + re.getMessage();
			logger.error(errorMsg, re);
			System.exit(1);
		}
		synchronized (SwangSecurityServerStarter.class) {
			while(running) {
				try {
					SwangSecurityServerStarter.class.wait();
				} catch (InterruptedException ie) {
					String errorMsg = serverName + "服务主线程等待异常！" + ie.getMessage();
					logger.error(errorMsg, ie);
				}
			}
		}
	}

	private static void logInfo(String content){
		if(logger.isInfoEnabled()){
			logger.info(content);
		}
	}
	
}
