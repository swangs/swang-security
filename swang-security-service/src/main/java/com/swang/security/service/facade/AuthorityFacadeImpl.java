package com.swang.security.service.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.swang.security.facade.AuthorityFacade;
import com.swang.security.facade.rpc.CommonResult;
import com.swang.security.facade.rpc.ResultFactory;
import com.swang.security.service.biz.AuthorityService;

/**
 * @ClassName: AuthorityFacadeImpl
 * @Description: 用户权限相关的对外rpc接口的实现
 * @author swang
 * @date 2017年4月22日 上午2:09:09
 */
@Component
public class AuthorityFacadeImpl implements AuthorityFacade {

	@Autowired
	private AuthorityService authorityService;
	
	@Override
	public CommonResult<List<String>> findAuthoritiesByUserId(Long userId) {
		if(userId==null){
			return ResultFactory.initCommonResultWithSuccess(null);
		}
		List<String> authorities = authorityService.findAuthoritiesByUserId(userId);
		return ResultFactory.initCommonResultWithSuccess(authorities);
	}

}
