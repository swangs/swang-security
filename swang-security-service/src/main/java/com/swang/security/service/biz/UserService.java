package com.swang.security.service.biz;

import com.swang.security.model.vo.UserDetailVo;

/**
 * @ClassName: UserService
 * @Description: 用户接口服务
 * @author swang
 * @date 2017年4月17日 下午7:58:02
 */
public interface UserService {

	/**
	 * 根据用户输入的用户名查找库中的用户信息
	 * @author swang
	 * @date 2017年4月17日 下午7:56:51
	 * @param userName	用户输入的用户名，可能是user_name、phone_no、email、extend_auth中的一种
	 * @return
	 */
	UserDetailVo findUserByUserName(String userName);
	
	
	
}
