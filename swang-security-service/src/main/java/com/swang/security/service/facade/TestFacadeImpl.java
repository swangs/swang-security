package com.swang.security.service.facade;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.swang.security.common.redis.RedisHandler;
import com.swang.security.facade.TestFacade;
import com.swang.security.facade.param.TestParam;
import com.swang.security.facade.result.TestResult;
import com.swang.security.facade.rpc.CommonResult;
import com.swang.security.facade.rpc.ContextParam;
import com.swang.security.facade.rpc.Result;
import com.swang.security.facade.rpc.ResultFactory;

@Component(value="testFacadeImpl")
public class TestFacadeImpl implements TestFacade {
	
	@Resource(name="redisHandler")
	private RedisHandler redisHandler;

	@Override
	public Result doTestResult(TestParam param) {
		//做些redis操作是事情测试玩玩
		String key = "test:redis:keyval";
		Map<String, String> hash = new HashMap<String, String>();
		hash.put("username", param.getUsername());
		hash.put("password", param.getPassword());
		hash.put("age", param.getAge()+"");
		redisHandler.hmset(key, hash, 300);
		
		String displayMsg = "调用成功啦！";
		return ResultFactory.initResultWithSuccess(displayMsg);
	}

	@Override
	public CommonResult<TestResult> findTestResult(TestParam param) {
		String displayMsg = "随便返回点什么，就返回个入参json吧！"+JSON.toJSONString(param);
		TestResult result = new TestResult();
		result.setAge(param.getAge()+100);
		result.setUsername(param.getUsername()+"王勇琳你好啊");
		return ResultFactory.initCommonResultWithSuccess(result, displayMsg);
	}

	@Override
	public CommonResult<TestResult> findTestResultByAuth(ContextParam<TestParam> param) {
		// 通常认证信息的校验会有框架通过AOP注解实现全局校验，此处仅为举例校验。
		String appId = param.getAppId();
		if(StringUtils.isBlank(appId) || appId.length()>32){
			String errorCode = "APPID_IS_INVALID";
			String errorDesc = "认证信息appId无效！";
			return CommonResult.toCommonResult(ResultFactory.initResultWithError(errorCode, errorDesc));
		}
		
		TestParam testParam = param.getContent();
		String displayMsg = "认证信息应该是成功了，随便返回点什么，就返回个入参json吧！"+JSON.toJSONString(param);
		TestResult result = new TestResult();
		result.setAge(testParam.getAge()+150);
		result.setUsername(testParam.getUsername()+"王勇琳你好啊");
		return ResultFactory.initCommonResultWithSuccess(result, displayMsg);
	}

}
