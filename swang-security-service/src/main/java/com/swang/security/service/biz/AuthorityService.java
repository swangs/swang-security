package com.swang.security.service.biz;

import java.util.List;

/**
 * @ClassName: AuthorityService
 * @Description: 用户权限相关的接口
 * @author swang
 * @date 2017年4月21日 下午9:05:11
 */
public interface AuthorityService {
	
	/**
	 * 根据用户ID查找用户的权限编码集合
	 * @author swang
	 * @date 2017年4月21日 下午9:06:26
	 * @param userId
	 * @return
	 */
	List<String> findAuthoritiesByUserId(Long userId);

}
