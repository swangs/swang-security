package com.swang.security.service.facade;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.swang.security.facade.UserFacade;
import com.swang.security.facade.result.UserDetailResult;
import com.swang.security.facade.rpc.CommonResult;
import com.swang.security.facade.rpc.ResultFactory;
import com.swang.security.model.vo.UserDetailVo;
import com.swang.security.service.biz.UserService;

/**
 * @ClassName: UserFacadeImpl
 * @Description: 暴露用户相关的RPC接口的封装实现
 * @author swang
 * @date 2017年4月18日 下午8:51:22
 */
@Component(value="userFacadeImpl")
public class UserFacadeImpl implements UserFacade {
	
	@Autowired
	private UserService userService;

	@Override
	public CommonResult<UserDetailResult> findUserByUserName(String userName) {
		if(StringUtils.isBlank(userName)){
			return ResultFactory.initCommonResultWithSuccess(null);
		}
		UserDetailVo vo = userService.findUserByUserName(userName);
		if(vo==null || vo.getUserId()==null || vo.getUserId()<=0){
			return ResultFactory.initCommonResultWithSuccess(null);
		}
		UserDetailResult result = new UserDetailResult();
		BeanUtils.copyProperties(vo, result);
		return ResultFactory.initCommonResultWithSuccess(result);
	}

}
