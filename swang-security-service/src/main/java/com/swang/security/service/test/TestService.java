package com.swang.security.service.test;

import com.swang.security.model.dto.TestDto;
import com.swang.security.model.vo.TestVo;

public interface TestService {
	
	TestVo findResultVo(TestDto dto);
	
}
