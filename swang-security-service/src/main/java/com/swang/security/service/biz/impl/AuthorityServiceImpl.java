package com.swang.security.service.biz.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swang.security.dal.AuthorityDao;
import com.swang.security.service.biz.AuthorityService;

/**
 * @ClassName: AuthorityServiceImpl
 * @Description: 用户权限相关的接口实现
 * @author swang
 * @date 2017年4月21日 下午9:08:22
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {
	
	@Autowired
	private AuthorityDao authorityDao;

	@Override
	public List<String> findAuthoritiesByUserId(Long userId) {
		List<String> functionCodes = authorityDao.findFunctionCodesByUserId(userId);
		List<String> dataAuthorites = authorityDao.findDataAuthoritesByUserId(userId);
		List<String> orgSupers = authorityDao.findOrgSupersByUserId(userId);
		List<String> authorities = new ArrayList<String>();
		if(functionCodes!=null && functionCodes.size()>0){
			authorities.addAll(functionCodes);
		}
		if(dataAuthorites!=null && dataAuthorites.size()>0){
			authorities.addAll(dataAuthorites);
		}
		if(orgSupers!=null && orgSupers.size()>0){
			authorities.addAll(orgSupers);
		}
		return authorities;
	}

}
