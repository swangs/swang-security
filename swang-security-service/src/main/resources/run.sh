#!/bin/sh
  
SERVER=/home/hswk/swang-security/swang-security-service
cd $SERVER
  
case "$1" in  

  start)  
    nohup java -Xmx384m -jar swang-security-service.jar -Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,address=63323,server=y,suspend=n >/dev/null 2>&1 &
    echo $! > $SERVER/swang-security-service.pid  
    ;;  
  
  stop)  
    kill `cat $SERVER/swang-security-service.pid`  
    rm -rf $SERVER/swang-security-service.pid  
    ;;  
  
  restart)  
    $0 stop  
    sleep 1  
    $0 start  
    ;;  
  
  *)  
    echo "Usage: run.sh {start|stop|restart}"  
    ;;  
  
esac 
exit 0
