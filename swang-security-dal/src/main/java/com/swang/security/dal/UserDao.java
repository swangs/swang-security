package com.swang.security.dal;

import com.swang.security.model.dto.UserDetailDto;
import com.swang.security.model.vo.UserDetailVo;

/**
 * @ClassName: UserDao
 * @Description: 用户持久化层
 * @author swang
 * @date 2017年4月17日 下午4:38:50
 */
public interface UserDao {
	
	UserDetailVo findUserByUserName(String userName);
	
	UserDetailVo findUserByPhoneNo(Long phoneNo);
	
	UserDetailVo findUserByEmail(String email);
	
	UserDetailVo findUserByExtendAuth(String extendAuth);
	
	void updateUserPhoneNo(UserDetailDto dto);
	
}
