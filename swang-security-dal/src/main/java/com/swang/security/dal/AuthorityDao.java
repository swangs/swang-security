package com.swang.security.dal;

import java.util.List;

/**
 * @ClassName: AuthorityDao
 * @Description: 用户权限相关的持久化层
 * @author swang
 * @date 2017年4月21日 下午9:14:54
 */
public interface AuthorityDao {

	/**
	 * 根据用户ID查找用户拥有的功能菜单及资源的权限编码集合
	 * @author swang
	 * @date 2017年4月21日 下午9:15:40
	 * @param userId
	 * @return
	 */
	List<String> findFunctionCodesByUserId(Long userId);
	
	/**
	 * 根据用户ID查找用户拥有的数据权限资源的权限编码的集合
	 * @author swang
	 * @date 2017年4月22日 上午1:34:45
	 * @param userId
	 * @return
	 */
	List<String> findDataAuthoritesByUserId(Long userId);
	
	/**
	 * 根据用户ID查找用户拥有的组织超级管理员
	 * @author swang
	 * @date 2017年4月22日 上午1:57:31
	 * @param userId
	 * @return
	 */
	List<String> findOrgSupersByUserId(Long userId);
	
}
