package com.swang.security.integration.common.retry;


import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName: RetryAspect
 * @Description: 重试请求切面
 * @author swang
 * @date 2017年4月10日 上午1:41:06
 */
@Component
@Aspect
public class RetryAspect {

	private static final Logger logger = Logger.getLogger(RetryAspect.class);

	/** 默认尝试执行次数 */
	@Value(value = "${integration.retry.num}")
	private int defaultRetryTimes = 3;

	@Around("@annotation(retry)")
	public Object around(ProceedingJoinPoint point, Retryable retry) throws Throwable {
		int retryTimes = defaultRetryTimes;
		if (retry.retryTimes() >= 0) {
			retryTimes = retry.retryTimes();
		}
		for (int i = 0; i <= retryTimes; i++) {
			try {
				return point.proceed();
			} catch (Exception e) {
				String message = "【Retry执行异常，第"+i+"次执行】";
				logger.error(message, e);
			}
		}
		return null;
	}

}
