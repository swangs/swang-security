package com.swang.security.integration.test;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.swang.redis.facade.TestFacade;
import com.swang.redis.facade.param.TestParam;
import com.swang.redis.facade.result.TestResult;
import com.swang.redis.facade.rpc.CommonResult;
import com.swang.security.model.dto.TestDto;
import com.swang.security.model.vo.TestVo;


/**
 * @ClassName: TestClient
 * @Description: 测试调用其他facade的测试客户端
 * @author swang
 * @date 2017年4月10日 上午12:22:09
 */
@Component
public class TestClient {
	
	@Resource(name="testFacade")
	private TestFacade testFacade;
	
//	@Retryable
	public TestVo findResultVo(TestDto dto){
		TestParam param = new TestParam();
		BeanUtils.copyProperties(dto, param, new String[]{});
		CommonResult<TestResult> result = testFacade.findTestResult(param);
		if (result == null || !result.getIsSuccess()) {
			return null;
		} else {
			TestVo vo = new TestVo();
			BeanUtils.copyProperties(result.getContent(), vo, new String[]{});
			return vo;
		}
	}
	
	
}
