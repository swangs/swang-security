package com.swang.security.demo.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.swang.security.demo.util.LoggerUtils;
import com.swang.security.sdk.SecurityContext;

@Controller
public class IndexController {
	private static final Logger logger = Logger.getLogger(IndexController.class);
    
    @RequestMapping(value="/")
	public String index(Model model){
    	LoggerUtils.debug(logger, "当前用户:{0}。是否匿名用户？{1}。是否认证过？{2}", SecurityContext.currentUser(), 
    			SecurityContext.isAnonymousUser(), SecurityContext.isAuthenticated());
    	if(!SecurityContext.isAuthenticated() || SecurityContext.isAnonymousUser()){
    		LoggerUtils.info(logger, "当前用户是匿名用户或未认证过。当前用户:{0}", SecurityContext.currentUser());
    		return "login";
    	}
    	model.addAttribute("currentUser", SecurityContext.currentUser());
    	model.addAttribute("currentAuthorities", SecurityContext.getAuthorities());
		return "index";
	}

    

}
