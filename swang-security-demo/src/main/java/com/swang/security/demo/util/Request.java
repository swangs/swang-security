package com.swang.security.demo.util;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Map;

/**
 * @ClassName: Request
 * @Description: 请求抽象基类
 * @author swang
 * @date 2017年4月18日 下午12:23:03
 * @param <T>
 */
public interface Request<T extends Response> extends Serializable {

    /**
     * @Title:getInterfaceName
     * @Description:获取接口名称 例如：/r/100/000
     * @param
     * @return:java.lang.String
     * @throws:
     */
    public String getInterfaceName();

    /**
     * @Title:getResponseClass
     * @Description:获取请求响应model
     * @param
     * @return:java.lang.Class<T>
     * @throws:
     */
    @JSONField(serialize = false)
    public Class<T> getResponseClass();

    /**
     * @Title:getRequestMode
     * @Description:获取请求的方法  GET/POST
     * @param
     * @return:java.lang.String
     * @throws:
     */
    public String getRequestMode();

    /**
     * @Title:isNeedSign
     * @Description:是否需要签名，如果需要签名，则在getSignParam中按照签名规定的顺序返回数据
     * @param
     * @return:boolean
     * @throws:
     */
    @JSONField(serialize = false)
    public boolean isNeedSign();

    /**
     * @Title:getBodyParam
     * @Description:在GET请求/需要签名的post请求中需要返回参数body
     * @param
     * @return:java.util.Map<java.lang.String,java.lang.Object>
     * @throws:
     */
    @JSONField(serialize = false)
    public Map<String, String> getBodyParam();

    /**
     * @Title:getJSONContent
     * @Description:获取请求的json串
     * @param
     * @return:java.lang.String
     * @throws:
     */
    @JSONField(serialize = false)
    public String getJSONContent();
    
}
