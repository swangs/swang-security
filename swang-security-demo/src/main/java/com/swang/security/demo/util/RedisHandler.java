package com.swang.security.demo.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.util.Pool;

/**
* @ClassName: RedisHandler
* @Description: redis的封装操作
* @author 王勇琳
* @date 2017年4月17日 下午2:17:04
*/
public class RedisHandler {
	
	private static final Logger logger = Logger.getLogger(RedisHandler.class);

    private Pool<Jedis> jedisPool;
    
    public void setJedisPool(Pool<Jedis> jedisPool) {
		this.jedisPool = jedisPool;
	}
    
    public Jedis getJedis() {
        return getJedis(0);
    }
    
    public Jedis getJedis(int failedNum) {
		Jedis jedis = null;
        if (failedNum < 11) {
            try {
                jedis = jedisPool.getResource();
            } catch (Exception e) {
                logger.error("get jedis error : " + e.getMessage(), e);
                if (jedis != null)
                    returnBrokenJedis(jedis);
                e.printStackTrace();
                try {
                    failedNum++;
                    Thread.sleep(1000 * failedNum);
                    jedis = getJedis(failedNum);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return jedis;
    }

    public Jedis bgetJedis() {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
        } catch (Exception e) {
            logger.error("get jedis error : " + e.getMessage(), e);
            returnBrokenJedis(jedis);
            e.printStackTrace();
            try {
                Thread.sleep(500);
                jedis = bgetJedis();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
        return jedis;
    }

    public void returnJedis(Jedis jedis) {
    	try{
    		//jedisPool.returnResource(jedis);
    		jedis.close();
    	}catch(JedisConnectionException e){
    		returnBrokenJedis(jedis);
    		//jedisPool.returnBrokenResource(jedis);
    	}catch(Exception e){
    		logger.error("return jedis occurs unexpected exception, jedis is:"+jedis.clientGetname(), e);
    		if(jedis!=null){
    			jedis.close();
    			jedis = null;
    		}
    	}
    }
    
    public void returnJedis(JedisPool theJedisPool, Jedis jedis) {
    	try{
    		//theJedisPool.returnResource(jedis);
    		jedis.close();
    	}catch(JedisConnectionException e){
    		returnBrokenJedis(theJedisPool, jedis);
    		//theJedisPool.returnBrokenResource(jedis);
    	}catch(Exception e){
    		logger.error("return jedis occurs unexpected exception, jedis is:"+jedis.clientGetname(), e);
    		if(jedis!=null){
    			jedis.close();
    			jedis = null;
    		}
    	}
    }
    public void returnBrokenJedis(Jedis jedis) {
    	try {
			//jedisPool.returnBrokenResource(jedis);
    		jedis.close();
		} catch (Exception e) {
			logger.error("return jedis occurs unexpected exception, jedis is:"+jedis.clientGetname(), e);
			if (jedis != null){
	        	jedis.close();
	        	jedis = null;
	        }
		}
    }
    
    public void returnBrokenJedis(JedisPool theJedisPool, Jedis jedis) {
    	try {
    		//theJedisPool.returnBrokenResource(jedis);
    		jedis.close();
		} catch (Exception e) {
			logger.error("return jedis occurs unexpected exception, jedis is:"+jedis.clientGetname(), e);
			if (jedis != null){
	        	jedis.close();
	        	jedis = null;
	        }
		}
    }

    public String get(String key) {
        Jedis jedis = getJedis();
        String value = null;
        try {
            value = jedis.get(key);
        } catch (Exception e) {
            logger.error("get value from redis error[key:" + key + "]", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return value;
    }

    public String get(String key, long timeout) {
        Jedis jedis = getJedis();
        String value = null;
        long t1 = System.currentTimeMillis();
        try {
            while (true) {
                value = jedis.get(key);
                if (StringUtils.isNotBlank(value))
                    break;
                if (System.currentTimeMillis() - t1 > timeout)
                    break;
                Thread.sleep(100);
            }
        } catch (Exception e) {
            logger.error("get value from redis error[key:" + key + "]", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return value;
    }

    public byte[] get(byte[] key) {
        Jedis jedis = getJedis();
        byte[] value = null;
        try {
            value = jedis.get(key);
        } catch (Exception e) {
            logger.error("get value from redis error[key:" + key + "]", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return value;
    }

    public byte[] get(byte[] key, long timeout) {
        Jedis jedis = getJedis();
        byte[] value = null;
        long t1 = System.currentTimeMillis();
        try {
            while (true) {
                value = jedis.get(key);
                if (value != null)
                    break;

                if (System.currentTimeMillis() - t1 > timeout)
                    break;
                Thread.sleep(100);
            }
        } catch (Exception e) {
            logger.error("get value from redis error[key:" + key + "]", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return value;
    }

    public void add(String key, String value) {
        add(key, value, 0, 0);
    }

    public void add(String key, String value, int expireTime) {
        add(key, value, expireTime, 0);
    }

    public void add(String key, String value, int expireTime, int failedNum) {
        if (failedNum < 3) {
            Jedis jedis = getJedis();
            try {
                jedis.set(key, value);
                if (expireTime > 0)
                    jedis.expire(key, expireTime);
            } catch (Exception e) {
                logger.error("add key[" + key + "] to redis error[" + failedNum
                        + "] ", e);
                returnBrokenJedis(jedis);
                add(key, value, ++failedNum);
            } finally {
                returnJedis(jedis);
            }
        }
    }

    public void add(byte[] key, byte[] value) {
        add(key, value, 0, 0);
    }

    public void add(byte[] key, byte[] value, int expireTime) {
        add(key, value, expireTime, 0);
    }

    public void add(byte[] key, byte[] value, int expireTime,
            int failedNum) {
        if (failedNum < 3) {
            Jedis jedis = getJedis();
            try {
                jedis.set(key, value);
                if (expireTime > 0)
                    jedis.expire(key, expireTime);
            } catch (Exception e) {
                logger.error("add key[" + key + "] to redis error[" + failedNum
                        + "] ", e);
                returnBrokenJedis(jedis);
                add(key, value, ++failedNum);
            } finally {
                returnJedis(jedis);
            }
        }
    }

    public void publish(String channel, String message) {
        publish(channel, message, 0);
    }

    public void publish(String channel, String message, int failedNum) {
        if (failedNum < 3) {
            Jedis jedis = getJedis();
            try {
                jedis.publish(channel, message);
            } catch (Exception e) {
                logger.error("publish message[" + message + "] to channel["
                        + channel + "] error[" + failedNum + "] : "
                        + e.getMessage());
                e.printStackTrace();
                returnBrokenJedis(jedis);
                publish(channel, message, ++failedNum);
            } finally {
                returnJedis(jedis);
            }
        }
    }

    public void queuePublish(String key, String channel, String message) {
        queuePublish(key, channel, message, 0);
    }

    public void queuePublish(String key, String channel, String message,
            int failedNum) {
        if (failedNum < 3) {
            Jedis jedis = getJedis();
            try {
                jedis.lpush(key, message);
                jedis.publish(channel, message);
            } catch (Exception e) {
                logger.error("queuePublish message[" + message
                        + "] to channel[" + channel + "] error[" + failedNum
                        + "] : " + e.getMessage());
                e.printStackTrace();
                returnBrokenJedis(jedis);
                queuePublish(key, channel, message, ++failedNum);
            } finally {
                returnJedis(jedis);
            }
        }
    }

    public void subscribe(JedisPubSub listener, String channel) {
        subscribe(listener, channel, 0);
    }

    public void subscribe(JedisPubSub listener, String channel,
            int failedNum) {
        Jedis jedis = getJedis();
        try {
            jedis.subscribe(listener, channel);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
            failedNum++;
            logger.error("subscribe error ! channel[" + channel
                    + "] failedNum[" + failedNum + "]");
            if (failedNum < 11) {
                try {
                    Thread.sleep(1000 * failedNum);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                subscribe(listener, channel, failedNum);
            }
        }
    }

    public void listAdd(String key, String... value) {
        Jedis jedis = getJedis();
        try {
            jedis.lpush(key, value);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
    }

    public String listPop(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.lpop(key);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return null;
    }

    public List<String> listAll(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.lrange(key, 0, -1);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return null;
    }

    public List<String> listPopAll(String key) {
        Jedis jedis = getJedis();
        try {
            long len = jedis.llen(key);
            List<String> list = new ArrayList<String>();
            for (int i = 0; i < len; i++) {
                list.add(jedis.lpop(key));
            }
            return list;
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return null;
    }

    /**
     * delete special value
     * @param key
     * @param count	delete numbers
     * @param value
     */
    public long listDel(String key, int count, String value) {
        Jedis jedis = getJedis();
        try {
            return jedis.lrem(key, count, value);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return 0;
    }

    public void listDelAll(String key) {
        Jedis jedis = getJedis();
        try {
            long len = jedis.llen(key);
            for (int i = 0; i < len; i++)
                jedis.rpop(key);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
    }

    /**
     * @param key
     * @param value
     * @return	1:add success	0:value is existed 	other:key is not a set type
     */
    public long setAdd(String key, String... value) {
        Jedis jedis = getJedis();
        try {
            return jedis.sadd(key, value);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return 0;
    }

    /**
     * 
     * @param key
     * @param value
     * @return	1:add success	0:value is existed 	other:key is not a set type
     */
    public long setDel(String key, String... value) {
        Jedis jedis = getJedis();
        try {
            return jedis.srem(key, value);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return 0;
    }

    public void setDelAll(String key) {
        Jedis jedis = getJedis();
        try {
            long total = jedis.scard(key);
            for (int i = 0; i < total; i++) {
                jedis.spop(key);
            }
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
    }

    public long setCount(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.scard(key);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return 0;
    }

    public Set<String> setAll(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.smembers(key);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
        return null;
    }

    public void expire(String key, int seconds) {
        Jedis jedis = getJedis();
        try {
            jedis.expire(key, seconds);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            e.printStackTrace();
        } finally {
            returnJedis(jedis);
        }
    }

    /**
    * 追加数据到已有的缓存数据 <br>
    * （基于保存Key,Map数据队列）
    * @return
    */
    public boolean addObjectInMap(String rkey, String mkey, Object value) {
        Jedis jedis = this.getJedis();
        try {
            jedis.hset(rkey, mkey, (String) value);

        } catch (Exception e) {
        } finally {
        	this.returnJedis(jedis);
        }
        return true;
    }

    /**
     * 从已有的缓存数据里面删除一个数据 <br>
     * （基于保存Key,Map数据队列）
     * @return
     */
    public Object removeObjectInMap(String rkey, String mkey) {
        Jedis jedis = this.getJedis();
        String value = null;
        try {
            value = jedis.hget(rkey, mkey);
            jedis.hdel(rkey, mkey);
        } catch (Exception e) {
        } finally {
        	this.returnJedis(jedis);
        }
        return value;
    }

    /**
     * 从已有的缓存数据里面获取一个数据 <br>
     * （基于保存Key,Map数据队列）
     * @return
     */
    public Object findObjectInMap(String rkey, String mkey) {
        Jedis jedis = this.getJedis();
        String value = null;
        try {
            value = jedis.hget(rkey, mkey);
        } catch (Exception e) {
        } finally {
        	this.returnJedis(jedis);
        }
        return value;
    }

    /**
     * @Description: redis  k-v 删除
     * @param key
     * @Date: 2015年7月31日 下午2:03:55
     */
    public void remove(String key) {
        Jedis jedis = getJedis();
        try{
        	jedis.del(key);
        }catch(Exception e){
        	returnBrokenJedis(jedis);
        }finally {
            returnJedis(jedis);
        }
    }

    public Set<String> hkeys(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.hkeys(key);
        } catch (Exception e) {
            logger.error("hkeys keys from redis error[key:" + key + "]", e);
            returnBrokenJedis(jedis);
            return null;
        } finally {
            returnJedis(jedis);
        }
    }
    
    public Set<String> keys(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.keys(key);
        } catch (Exception e) {
            logger.error("keys from redis error[key:" + key + "]", e);
            returnBrokenJedis(jedis);
            return null;
        } finally {
            returnJedis(jedis);
        }
    }
    
    public boolean exists(String key) {
        Jedis jedis = getJedis();
        try {
            return jedis.exists(key);
        } catch (Exception e) {
            returnBrokenJedis(jedis);
            return false;
        } finally {
            returnJedis(jedis);
        }
    }
    
    public void hmset(String key,Map<String, String> hash){
	    hmset(key, hash, 0, 0);
    }
	
	public void hmset(String key,Map<String, String> hash,int expireTime){
	    hmset(key, hash, expireTime, 0);
	}
	
	public void hmset(String key,Map<String, String> hash,int expireTime,int failedNum){
        if(failedNum < 3){
            Jedis jedis = getJedis();
            try {
                jedis.hmset(key, hash);
                if(expireTime > 0)
                    jedis.expire(key, expireTime);
            } catch (Exception e) {
                logger.error("hmset key["+key+"] to redis error["+failedNum+"] ", e);
                returnBrokenJedis(jedis);
                hmset(key, hash, expireTime, ++failedNum);
            } finally {
                returnJedis(jedis);
            }
        }
    }
	
	public List<String> hmget(String key,long timeout,String... field){
	    Jedis jedis = getJedis();
        List<String> value = null;
        long t1 = System.currentTimeMillis();
        try {
            while (true) {
                value = jedis.hmget(key, field);
                if (value!=null&&value.size()>0)
                    break;
                if (System.currentTimeMillis() - t1 > timeout)
                    break;
                Thread.sleep(1);
            }
        } catch (Exception e) {
            logger.error("hmget [field:"+ field.toString() +"] value from redis error[key:"+key+"]", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return value;
	}
	public Map<String,String> hgetAll(String key,long timeout ){
	    Jedis jedis = getJedis();
	    Map<String,String> result = null;
        long t1 = System.currentTimeMillis();
        try {
            while (true) {
            	result=jedis.hgetAll(key);
                if (result!=null&&!result.isEmpty())
                    break;
                if (System.currentTimeMillis() - t1 > timeout)
                    break;
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            logger.error("hgetAll [key:"+key +"] value from redis error ", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return result;
	}
	
	public String hget(String key,String field, long timeout){
	    Jedis jedis = getJedis();
	    String result = null;
        long t1 = System.currentTimeMillis();
        try {
            while (true) {
            	result=jedis.hget(key,field);
                if (result!=null&&!result.isEmpty())
                    break;
                if (System.currentTimeMillis() - t1 > timeout)
                    break;
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            logger.error("hgetAll [key:"+key +"] value from redis error ", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return result;
	}
	public boolean hExist(String key,String field){
	    Jedis jedis = getJedis();
	    try {
	    	 return jedis.hexists(key, field);
		} catch (Exception e) {
			logger.error("hexist [field:"+ field.toString() +"] value from redis error[key:"+key+"]", e);
            returnBrokenJedis(jedis);
        } finally {
            returnJedis(jedis);
        }
        return false;
	}
	
    public void hset(String key,String field,String value){
        Jedis jedis= getJedis();
        try {
            jedis.hset(key,field,value);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            returnJedis(jedis);
        }
    }
    
    /**
     * @Description: 从hash表删除filed
     * @param key
     * @param field
     * @Date: 2015年11月12日 下午3:02:56
     */
    public void hdel(String key,String field){
    	Jedis jedis= getJedis();
        try {
            jedis.hdel(key,field);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            returnJedis(jedis);
        }
    }
    
    /**
	 * redis 排序
	 * @param key
	 * @return
	 */
	public List<String> sort(String key){
		Jedis jedis = getJedis();
		List<String> sortList = new ArrayList<String>();
		try {
			sortList = jedis.sort(key);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		} finally {
			returnJedis(jedis);
		}
		return sortList;
	}
	
	/**
	 * 有序set添加
	 * @param key
	 * @param score
	 * @param member
	 */
	public void zadd(String key,double score,String member){
		Jedis jedis = getJedis();
		try {
			jedis.zadd(key, score, member);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
	}
	
	/**
	 * 有序正向set集合
	 * @param key
	 * @return
	 */
	public Set<String> setSortForward(String key){
		Jedis jedis = getJedis();
		Set<String> setRecord=new  HashSet<String>();
		try {
			setRecord=jedis.zrange(key, 0, -1);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
		return setRecord;
	}
	
	/**
	 * 有序反向set集合
	 * @param key
	 * @return
	 */
	public Set<String> setSortBackward(String key){
		Jedis jedis = getJedis();
		Set<String> setRecord=new  HashSet<String>();
		try {
			setRecord =jedis.zrevrange(key, 0, -1);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
		return setRecord;
	}
	
	/**
	 *  有序set删除
	 * @param key
	 * @param member
	 */
	public void zdel(String key,String member){
		Jedis jedis = getJedis();
		try {
			jedis.zrem(key, member);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
	}
	
	/**
	 * set集合 index值（0，1，2，……）
	 * @param key
	 * @return
	 */
	public Long indexSet(String key,String member){
		Jedis jedis = getJedis();
		Long index=null;
		try {
			index= jedis.zrank(key, member);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
		return index;
	}
	
	/**
	 * set集合 第N的值（0，1，2，……）
	 * @param key
	 * @return
	 */
	public Set<String> getIndexSet(String key,Long endIndex){
		Jedis jedis = getJedis();
		Set<String> set=new HashSet<String>();
		try {
			set= jedis.zrange(key, 0, endIndex);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
		return set;
	}
	
	/**
	 * set 获取分数满足表达式startScore < score <= endScore的成员
	 * @param key
	 * @param startScore
	 * @param endScore
	 * @return
	 */
	public Set<String> getByScoreSet(String key,Long startScore,Long endScore){
		Jedis jedis = getJedis();
		Set<String> set=new HashSet<String>();
		try {
			set= jedis.zrangeByScore(key, startScore, endScore);
		} catch (Exception e) {
			e.printStackTrace();
			returnBrokenJedis(jedis);
		}finally {
			returnJedis(jedis);
		}
		return set;
	}
	
	public void rpush(String key,String value){
		Jedis jedis = getJedis();
		try {
			jedis.rpush(key, value);
		} catch (Exception e) {
			returnBrokenJedis(jedis);
			e.printStackTrace();
		} finally {
			returnJedis(jedis);
		}
	}
	
	public long llen(String key){
		Jedis jedis = getJedis();
		long value = 0;
		try {
			value = jedis.llen(key);
		} catch (Exception e) {
			logger.error("get value from redis error[key:"+key+"]", e);
			returnBrokenJedis(jedis);
		} finally {
			returnJedis(jedis);
		}
		
		return value;
	}
	
	public void listAddAfterLeft(String key, String ... value){
		Jedis jedis = getJedis();
		try {
			jedis.lpush(key, value);
		} catch (Exception e) {
			returnBrokenJedis(jedis);
			e.printStackTrace();
		} finally {
			returnJedis(jedis);
		}
	}
	
	public String blistPop(String key, int expireTime){
		Jedis jedis = getJedis();
		try {
			List<String> list = jedis.blpop(expireTime,key);
			if(list!=null&&list.size()==2){
				return list.get(1);
			}
		} catch (Exception e) {
			returnBrokenJedis(jedis);
			e.printStackTrace();
		} finally {
			returnJedis(jedis);
		}
		return null;
	}
	
	public void addExpireTime(String key, String value, int expireTime){
		addExpire(key, value, expireTime, 0);
	}
	
	public void addExpire(String key, String value, int expireTime, int failedNum){
		if(failedNum < 3){
			Jedis jedis = getJedis();
			try {
				jedis.set(key, value);
				if(expireTime > 0)
					jedis.expire(key, expireTime);
			} catch (Exception e) {
				logger.error("add key["+key+"] to redis error["+failedNum+"] ", e);
				returnBrokenJedis(jedis);
				add(key, value, expireTime, ++failedNum);
			} finally {
				returnJedis(jedis);
			}
		}
	}
	
	/**
	 * 获取map的key对应的value
	 * @param key
	 * @return
	 */
	public List<String> getAllMapValue(String key){
		Jedis jedis = getJedis();
		try {
			List<String> list = jedis.hvals(key);
			if(null != list){
				return list;
			}else{
				return null;
			}
		} catch (Exception e) {
			returnBrokenJedis(jedis);
			e.printStackTrace();
		} finally {
			returnJedis(jedis);
		}
		return null;
	}
	
}
