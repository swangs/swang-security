package com.swang.security.demo.util;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 * @ClassName: Response
 * @Description: 请求响应的抽象基类
 * @author swang
 * @date 2017年4月18日 下午12:24:04
 */
public abstract class Response implements Serializable {

    private static final long serialVersionUID = 8821983119253316465L;

    @JSONField(name="kind")
    private String code;

    @JSONField(name="msg")
    private String msg;

    @JSONField(name="result")
    private boolean result;

    public Response(){

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public boolean isSuccess(){
        return result;
    }
}
