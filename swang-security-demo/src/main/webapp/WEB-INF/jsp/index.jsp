<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="common/common.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
	<title>欢迎您！这是首页</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="${contextPath}/js/jQuery/jquery-1.9.1.min.js" type="text/javascript" charset="UTF-8"></script>
	<!--[if lt IE 9]>
		<script src="${contextPath}/js/json2.js" type="text/javascript" charset="UTF-8"></script>
	<![endif]-->
	<script type="text/javascript">
	$(function(){
		
	});
	</script>
</head>
<body>
	<a id="logout" href="${contextPath}/logout">退出登录</a><br/><br/>
	欢迎您！这是首页<br/>
	${contextPath}<br/>
	${basePath}<br/><br/>
	当前用户信息：${currentUser}，欢迎您！<br/><br/>
	当前用户拥有的权限：${currentAuthorities}<br/>
	<br/><br/>
	<a id="testAuthOne" href="${contextPath}/r/testAuthOne" target="blank">测试权限一</a><br/>
	<a id="testAuthTwo" href="${contextPath}/r/testAuthTwo" target="blank">测试权限二</a><br/>
	
</body>
</html>
