<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath;
%>
<%-- 上下文路径：'/projectName' --%>
<c:set var="contextPath" value="<%=contextPath%>"/>
<%-- 全文路径：'http://localhost:8080/projectName' --%>
<c:set var="basePath" value="<%=basePath%>"/>
<script type="text/javascript">
<!--
var globalContextPath = '${contextPath}';
var globalBasePath = '${basePath}';
//-->
</script>
