package com.swang.security.facade;

import java.util.List;

import com.swang.security.facade.rpc.CommonResult;

/**
 * @ClassName: AuthorityFacade
 * @Description: 用户权限相关的对外rpc接口
 * @author swang
 * @date 2017年4月22日 上午2:08:22
 */
public interface AuthorityFacade {

	/**
	 * 根据用户ID查找用户的权限编码集合
	 * @author swang
	 * @date 2017年4月22日 上午2:08:05
	 * @param userId
	 * @return
	 */
	CommonResult<List<String>> findAuthoritiesByUserId(Long userId);
	
}
