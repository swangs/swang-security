package com.swang.security.facade.rpc;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: CommonError
 * @Description: 普通错误基础模型
 * @author swang
 * @date 2017年4月9日 上午10:18:22
 */
public class CommonError {
	
	/** 错误编码 */
	private String errorCode;
	
	/** 错误描述 */
	private String errorDesc;
	
	/** 错误来源，表示该错误来源于哪一套RPC服务 */
	private String location;
	
	public CommonError(){
	}
	
	public CommonError(String errorCode, String errorDesc) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}
	
	public CommonError(String errorCode, String errorDesc, String location) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
		this.location = location;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
