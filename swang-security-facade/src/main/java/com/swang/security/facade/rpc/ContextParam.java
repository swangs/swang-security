package com.swang.security.facade.rpc;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: ContextParam
 * @Description: 带上下文参数（比如一些认证信息信息）的入参
 * @author swang
 * @date 2017年4月9日 上午11:00:10
 * @param <T>
 */
public class ContextParam<T> extends ContextBean  {
	private static final long serialVersionUID = 1363080936010957504L;

	/** 业务参数内容 */
	private T content;
	
	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
