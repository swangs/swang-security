package com.swang.security.facade;

import com.swang.security.facade.param.TestParam;
import com.swang.security.facade.result.TestResult;
import com.swang.security.facade.rpc.CommonResult;
import com.swang.security.facade.rpc.ContextParam;
import com.swang.security.facade.rpc.Result;

/**
 * @ClassName: TestFacade
 * @Description: 用于测试使用的rpc接口
 * @author swang
 * @date 2017年4月9日 上午10:16:28
 */
public interface TestFacade {

	/**
	 * 普通rpc接口调用，无业务返回结果
	 * @author swang
	 * @date 2017年4月9日 上午11:37:38
	 * @param param
	 * @return
	 */
	Result doTestResult(TestParam param);
	
	/**
	 * 普通rpc接口调用
	 * @author swang
	 * @date 2017年4月9日 上午10:15:46
	 * @param param
	 * @return
	 */
	CommonResult<TestResult> findTestResult(TestParam param);
	
	/**
	 * 通过上下文认证参数来调用rpc接口
	 * @author swang
	 * @date 2017年4月9日 上午10:15:19
	 * @param param
	 * @return
	 */
	CommonResult<TestResult> findTestResultByAuth(ContextParam<TestParam> param);
	
}
