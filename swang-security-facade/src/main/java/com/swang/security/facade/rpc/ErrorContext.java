package com.swang.security.facade.rpc;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: ErrorContext
 * @Description: 错误上下文，错误堆栈
 * @author swang
 * @date 2017年4月9日 上午11:03:18
 */
public class ErrorContext {
	
	/** 错误堆栈 */
	private List<CommonError> errorStack = new ArrayList<CommonError>();
	
	public ErrorContext(){
	}
	
	public ErrorContext(List<CommonError> errorStack) {
		this.errorStack = errorStack;
	}

	public List<CommonError> getErrorStack() {
		return errorStack;
	}

	public void setErrorStack(List<CommonError> errorStack) {
		this.errorStack = errorStack;
	}
	
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
