package com.swang.security.facade.result;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: UserDetailVo
 * @Description: 数据库查询出的用户详情
 * @author swang
 * @date 2017年4月17日 下午7:18:52
 */
public class UserDetailResult implements Serializable {
	private static final long serialVersionUID = -3220734093494759179L;

	/** 对应数据库的主键 */
	private Long userId;
	
	/** 登录用户名 */
	private String username;

	/** 登录密码 */
	private String password;
	
	/** 密码盐值 */
	private String salt;
	
	/** 用户状态（1=正常、2=冻结、3=注销）；注销表示用户被注销不能用，但是该用户数据在统计范围之内 */
	private Integer status;
	
	/** 是否启用,1启用 0停用；停用所有数据将失效包括统计数据 */
	private boolean enabled;
	
	/** 用户头像 */
	private String headPhoto;
	
	/** 昵称 */
	private String nickName;
	
	/** 人员Id */
	private Long personId;
	
	

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getHeadPhoto() {
		return headPhoto;
	}

	public void setHeadPhoto(String headPhoto) {
		this.headPhoto = headPhoto;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
