package com.swang.security.facade;

import com.swang.security.facade.result.UserDetailResult;
import com.swang.security.facade.rpc.CommonResult;

/**
 * @ClassName: UserFacade
 * @Description: 暴露用户相关的RPC接口
 * @author swang
 * @date 2017年4月18日 下午8:50:16
 */
public interface UserFacade {

	/**
	 * 根据用户输入的用户名查找库中的用户信息
	 * @author swang
	 * @date 2017年4月17日 下午7:56:51
	 * @param userName	用户输入的用户名，可能是user_name、phone_no、email、extend_auth中的一种
	 * @return
	 */
	CommonResult<UserDetailResult> findUserByUserName(String userName);
	
	
}
