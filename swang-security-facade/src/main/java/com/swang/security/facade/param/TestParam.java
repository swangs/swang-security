package com.swang.security.facade.param;

import java.io.Serializable;

public class TestParam implements Serializable {
	private static final long serialVersionUID = -2143561492040538654L;

	private String username;
	
	private String password;
	
	private Integer age;
	
	public TestParam(){}

	public TestParam(String username, String password, Integer age) {
		this.username = username;
		this.password = password;
		this.age = age;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
}
