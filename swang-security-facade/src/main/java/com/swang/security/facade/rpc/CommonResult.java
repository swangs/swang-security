package com.swang.security.facade.rpc;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: CommonResult
 * @Description: 通用的常用的返回结果
 * @author swang
 * @date 2017年4月9日 上午10:53:34
 * @param <T>
 */
public class CommonResult<T> extends Result {
	private static final long serialVersionUID = -4240896825256128290L;
	
	private T content;
	
	/**
	 * 将父类转换成子类
	 * @author 王勇琳
	 * @date 2016年7月14日 上午10:18:18
	 * @param result
	 * @return
	 */
	public static <T> CommonResult<T> toCommonResult(Result result){
		CommonResult<T> cr = new CommonResult<T>();
		cr.setIsSuccess(result.getIsSuccess());
		cr.setErrorCode(result.getErrorCode());
		cr.setErrorDesc(result.getErrorDesc());
		cr.setDisplayMsg(result.getDisplayMsg());
		cr.setErrorContext(result.getErrorContext());
		cr.setErrorParameters(result.getErrorParameters());
		return cr;
	}
	
	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}
	
	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
