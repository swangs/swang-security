package com.swang.security.facade.rpc;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: ContextBean
 * @Description: 上下文参数，比如一些接口访问认证信息（可扩展）
 * @author swang
 * @date 2017年4月9日 上午10:55:17
 */
public class ContextBean implements Serializable  {
	private static final long serialVersionUID = -304146729740921774L;

	/** 支付宝用户id，最大长度32 */
	private String appId;
	
	

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
