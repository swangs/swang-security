package com.swang.security.facade.rpc;

import java.util.List;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName: PageResult
 * @Description: 返回结果
 * @author swang
 * @date 2017年4月9日 上午11:04:52
 * @param <T>	返回结果的业务数据
 */
public class PageResult<T> extends Result{
	private static final long serialVersionUID = -3343893909519199619L;

	/** 当前页数 */
	private int currentPageno;
	/** 总页数 */
	private int totalPageno;
	
	/** 返回结果的业务数据 */
	private List<T> content;
	
	/**
	 * 将父类转换成子类
	 * @author 王勇琳
	 * @date 2016年7月14日 上午10:18:50
	 * @param result
	 * @return
	 */
	public static <T> PageResult<T> toPageResult(Result result){
		PageResult<T> pr = new PageResult<T>();
		pr.setIsSuccess(result.getIsSuccess());
		pr.setErrorCode(result.getErrorCode());
		pr.setErrorDesc(result.getErrorDesc());
		pr.setDisplayMsg(result.getDisplayMsg());
		pr.setErrorContext(result.getErrorContext());
		pr.setErrorParameters(result.getErrorParameters());
		return pr;
	}

	public int getCurrentPageno() {
		return currentPageno;
	}

	public void setCurrentPageno(int currentPageno) {
		this.currentPageno = currentPageno;
	}

	public int getTotalPageno() {
		return totalPageno;
	}

	public void setTotalPageno(int totalPageno) {
		this.totalPageno = totalPageno;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public String toString() {
		return JSON.toJSONString(this);
	}
	
}
