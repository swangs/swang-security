package com.swang.security.facade.rpc;

/**
 * @ClassName: ParamFactory
 * @Description: 业务参数构建工厂
 * @author swang
 * @date 2017年4月9日 上午11:05:54
 */
public class ParamFactory {
	
	/**
	 * 初始化一个上下文入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:15:03
	 * @param userId	支付宝用户id
	 * @return
	 */
	public static <T> ContextParam<T> initContextParam(String appId, T content){
		ContextParam<T> param = new ContextParam<T>();
		param.setAppId(appId);
		param.setContent(content);
		return param;
	}
	
	/**
	 * 初始化一个上下文入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:15:03
	 * @param userId	支付宝用户id
	 * @return
	 */
	public static <T> ContextParam<T> initContextParam(ContextBean contextBean, T content){
		ContextParam<T> param = new ContextParam<T>();
		param.setAppId(contextBean.getAppId());
		
		param.setContent(content);
		return param;
	}
	
	/**
	 * 初始化分页参数入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:15:18
	 * @param content	业务数据入参
	 * @param pageNo	页码
	 * @param limit		每页多少条
	 * @return
	 */
	public static <T> PageParam<T> initPageParam(T content, Integer pageNo, Integer limit){
		PageParam<T> param = new PageParam<T>();
		param.setPageNo(pageNo);
		param.setLimit(limit);
		param.setContent(content);
		return param;
	}
	
	/**
	 * 初始化上下文及分页参数入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:15:43
	 * @param contextParam	上下文入参，如用户信息等
	 * @param pageParam		分页入参
	 * @return
	 */
	public static <T> PageContextParam<T> initPageContextParam(ContextBean contextBean, PageParam<T> pageParam){
		PageContextParam<T> pageContextParam = new PageContextParam<T>();
		pageContextParam.setAppId(contextBean.getAppId());
		
		pageContextParam.setPageParam(pageParam);
		return pageContextParam;
	}
	
	/**
	 * 初始化上下文及分页参数入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:16:06
	 * @param contextParam
	 * @param content	业务数据入参
	 * @param pageNo	页码
	 * @param limit		每页多少条
	 * @return
	 */
	public static <T> PageContextParam<T> initPageContextParam(ContextBean contextBean, T content, Integer pageNo, Integer limit){
		PageContextParam<T> pageContextParam = new PageContextParam<T>();
		pageContextParam.setAppId(contextBean.getAppId());
		
		PageParam<T> pageParam = new PageParam<T>();
		pageParam.setPageNo(pageNo);
		pageParam.setLimit(limit);
		pageParam.setContent(content);
		pageContextParam.setPageParam(pageParam);
		return pageContextParam;
	}
	
	/**
	 * 初始化上下文及分页参数入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:16:13
	 * @param userId	支付宝用户id
	 * @param pageParam	分页入参
	 * @return
	 */
	public static <T> PageContextParam<T> initPageContextParam(String appId, PageParam<T> pageParam){
		PageContextParam<T> pageContextParam = new PageContextParam<T>();
		pageContextParam.setAppId(appId);
		pageContextParam.setPageParam(pageParam);
		return pageContextParam;
	}
	
	/**
	 * 初始化上下文及分页参数入参
	 * @author 王勇琳
	 * @date 2016年7月11日 下午8:16:18
	 * @param userId	支付宝用户id
	 * @param content	业务数据入参
	 * @param pageNo	页码
	 * @param limit		每页多少条
	 * @return
	 */
	public static <T> PageContextParam<T> initPageContextParam(String appId, T content, Integer pageNo, Integer limit){
		PageContextParam<T> pageContextParam = new PageContextParam<T>();
		pageContextParam.setAppId(appId);
		PageParam<T> pageParam = new PageParam<T>();
		pageParam.setPageNo(pageNo);
		pageParam.setLimit(limit);
		pageParam.setContent(content);
		pageContextParam.setPageParam(pageParam);
		return pageContextParam;
	}
	
}
